<?php
/**
 * @file
 * Admin forms for Color Per node module.
 */
function color_per_node_type_form($form = array(), &$form_state, $node_type) {

  // Assemble a unique variable key for this node type.
  $type_key = 'color_per_node_type_' . $node_type->type;

  // Store the variable key in the form state.
  $form['type_key'] = array(
    '#type' => 'value',
    '#value' => $type_key,
  );

  // Store the node type in the form state.
  $form['type'] = array(
    '#type' => 'value',
    '#value' => $node_type->type,
  );

  // On AJAX refreshes use the form state instead of the variable.
  if (isset($form_state['values']) && isset($form_state['values'][$type_key])) {
    $node_type_colors = $form_state['values'][$type_key];
  }
  else {
    $node_type_colors = variable_get($type_key, array());
  }

  // Create an ajax holder that will house the settings table.
  $form[$type_key] = array(
    '#tree' => TRUE,
    '#prefix' => '<div id="color-add-holder">',
    '#suffix' => '</div>',
    '#type' => 'cpn_settings_table',
  );

  // Automatically add a colour to empty settings.
  if (empty($node_type_colors)) {
    $node_type_colors[] = _color_per_node_prepare_new_color();
  }

  // Loop through each color, adding fields for all the properties.
  foreach ($node_type_colors as $delta => $color) {
    $form[$type_key][$delta]['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#default_value' => $color['title'],
      '#description' => t('Shown to the end user on the node form.'),
      '#title_display' => 'invisible',
      '#required' => TRUE,
    );
    $form[$type_key][$delta]['selector'] = array(
      '#type' => 'textfield',
      '#title' => t('CSS Selector(s)'),
      '#default_value' => $color['selector'],
      '#description' => t('e.g. "body" or "h1" or leave empty to affect whole node'),
      '#title_display' => 'invisible',
      '#required' => FALSE,
    );
    $form[$type_key][$delta]['attribute'] = array(
      '#type' => 'textfield',
      '#title' => t('Attribute'),
      '#default_value' => $color['attribute'],
      '#description' => t('e.g. "color", "background-color" or "border-color"'),
      '#title_display' => 'invisible',
      '#required' => TRUE,
    );
    $form[$type_key][$delta]['teaser'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use on teasers?'),
      '#default_value' => $color['teaser'],
      '#title_display' => 'invisible',
    );
    $form[$type_key][$delta]['default'] = array(
      '#type' => 'textfield',
      '#title' => t('Default color'),
      '#default_value' => $color['default'],
      '#size' => 6,
      '#title_display' => 'invisible',
      '#attached' => array(
        // Add jQuery simple color
        'library' => array(
          array('color_per_node', 'bgrins-spectrum'),
        ),
        // Add custom JavaScript.
        'js' => array(
          drupal_get_path('module', 'color_per_node') . '/color_per_node.admin.js',
        ),
      ),
      '#attributes' => array(
        'class' => array('color-per-node-picker'),
      ),
    );
    $form[$type_key][$delta]['remove_style'] = array(
      '#type' => 'checkbox',
      '#title' => 'x',
      '#title_display' => 'invisible',
    );
  }

  // Adds new rows and validates existing rows.
  $form['add_another'] = array(
    '#type' => 'submit',
    '#value' => t('Add another'),
    '#submit' => array('color_per_node_add_nojs'),
    '#ajax' => array(
      'callback' => 'color_per_node_add',
      'wrapper' => 'color-add-holder',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  return system_settings_form($form);

}

/**
 * Implements Form API validation.
 * Removes 'remove style' styles.
 */
function color_per_node_type_form_validate(&$form, &$form_state) {

  $type_key = $form_state['values']['type_key'];
  $type = $form_state['values']['type'];

  foreach ($form_state['values'][$type_key] as $delta => $color_style) {
    if ($color_style['remove_style'] == 1) {
      unset($form_state['values'][$type_key][$delta]);
      db_delete('color_per_node')
        ->condition('type', $type)
        ->condition('delta', $delta)
        ->execute();
    }
  }
}

/**
 * AJAX callback for adding a new color to the node type.
 */
function color_per_node_add(&$form, &$form_state) {
  $type_key = $form_state['values']['type_key'];
  $form_state['values'][$type_key][] = _color_per_node_prepare_new_color();
  $form = drupal_rebuild_form('color_per_node_type_form', $form_state, $form);
  return $form[$type_key];
}

/**
 * Submit handler for non-javascript fallback to add another color.
 */
function color_per_node_add_nojs(&$form, &$form_state) {
  $type_key = $form_state['values']['type_key'];

  $node_type_colors = $form_state['values'][$type_key];

  $node_type_colors[] = _color_per_node_prepare_new_color();

  variable_set($type_key, $node_type_colors);

}

/**
 * AJAX callback for adding a new color to the node type.
 */
function color_per_node_remove(&$form, &$form_state) {
  $type_key = $form_state['values']['type_key'];
  $delta = $form_state['triggering_element']['#parents'][1];

  unset($form_state['values'][$type_key][$delta]);
  $form = drupal_rebuild_form('color_per_node_type_form', $form_state, $form);

  return $form[$type_key];
}

/**
 * Prepares a new, blank color type.
 */
function _color_per_node_prepare_new_color() {
  return array(
    'selector' => '',
    'attribute' => '',
    'teaser' => 1,
    'default' => '',
  );
}
