/**
 * @file
 * Attaches the behaviors for the color picker to fields.
 */

(function ($) {

Drupal.behaviors.colorPerNode = {
  attach: function (context, settings) {
    $('.color-per-node-picker', context).spectrum({
      showInput: true,
      preferredFormat: "hex"
    });
  }
}

})(jQuery);
