/**
 * @file
 * README file for Color Per Node
 */

Color Per Node
Allows content editors to change colors for administrator defined styles of
individual nodes.

===============================================================================
INSTRUCTIONS
===============================================================================

1. Navigate to the types page (admin/structure/types) and click 'edit' for the
   type you'd like to assign colors to.

2. Click the 'Colors' tab.

3. Add styles to the node type:
   - Title:         The administrative title shown to content editors.
   - Selector:      The CSS selector that the attribute will apply to.
   - Attribute:     The CSS attribute the color will apply to.
   - Use on teaser: Should the colors influence teasers as well as node pages?
   - Default:       The default color if the user doesn't enter a value.
   - Remove:        Delete this style (and all associated node colors).

4. Click save configuration.

5. Edit or create a node of the given type and under 'Colors' (in the vertical
   tabs) change the colors for each style.

6. View the node page and the styles should be visible.

7. To debug the styles, view source and look for the inline style declaration in
   the page head.
